# Lanelet Messages
Here the custom messages are implemented, which are used for the information transfer between lane_detection and path_conversion.
## The Message Types
The idea behind lanelet messages is to ensure an orderly flow of information between two nodes, with the publisher sending two point lists of metadata to the subscriber. The metadata is stored in the header. The two point lists should essentially represent points on a lane.
### Lanelet.msg
std_msgs/Header meta_info
LineString left_boundary
LineString right_boundary
### LineString.msg
geometry_msgs/Point[] line_string

### CMakeLists.txt
This package must be built with the build_type ament_cmake in order to create the custom messages correctly. Furthermore, all externally visible message types must be created here, the builtin_interfaces and rosidl_default_generators are mandatory for this. In addition, the message types implemented here depend on those already found in ros2, which are therefore required as dependencies.
### package.xml
Here some dependencies are given which are needed. In addition to the various message types on which the ones included here are based, the builtin_interfaces must also be used.